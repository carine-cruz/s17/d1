//console.log(`JS Arrays`);

/*
	ARRAY
		- collection of multiple related data/values
*/

let month = [`Jan`,`Feb`,`Mar`,`Apr`,`May`,`June`];

console.log(month);			//return array
console.log(month.length);  //return length
console.log(month[3]);		//return content of position 3 )zero-based

//using for loop
/*for(let i = 0; i < month.length; i++){
	console.log(`Month of ${month[i]}`); //display each element in an array
}
*/
// let data = [`John`, 85, true, {id:`123`}, null, undefined];
// console.log(data);

//How to initialize an array?
	// let arr1 = [];
	// let arr1 = [element, element.. element];

	let fruits = [`apple`, `banana`, `strawberry`];

	//let arr1 = new Array (elements, elements.. elements);
	let phones = new Array(`iphone`,`samsung`,`nokia`);

	/*console.log(phones);*/

	/*
		Array Manipulation
	*/

		let count = [`one`,`two`,`three`,`four`,`five`];
		console.log(count);

		//Push() method
			//adds element at the end of the array
			//can ad multiple elements
				//count.push(`six`,`seven`,`eight`);

/*			count.push(`six`);
			console.log(count);

			//using a function
			function Push(element){
				count.push(element);
			}

			Push(`seven`);
			console.log(count);

			Push(`eight`);
			console.log(count);*/

		//Pop() method
			//removes last element from array
/*			count.pop();
			console.log(count);

			function Pop(){
				count.pop();
			}

			Pop();
			console.log(count);

			Pop();
			console.log(count);
*/
		//Shift() method
			//removes an element at the beginning of an array
/*			count.shift();
			console.log(count);*/

		//Unshift() method
			//add an element at the beginning of an array
/*			count.unshift(`one`);
			console.log(count);*/

		let digits = [5, `one`, 8, 6, 3, 9, 4, 500, 840,239, `ten`];

		//Sort() method
			digits.sort();
			//console.log(digits);

			digits.sort( 
				function(a, b){
					//ascending order
					//return a - b
					//not arithmetic operation, just a dash to indicate how sorting is

					//descending order
					return b - a
				}
			)

			//console.log(digits);

/*
	Slice and Splice
*/

	//Splice
		//modifies the original array and returns a new array containing deleted elements
		//takes 3 arguments
		//1st parameter - index number (start of remove)
		//2nd parameter - # of elements to be deleted
		//3rd parameter - new elements to be added in placement of deleted elements

/*		console.log(month);*/

	// let nSplice = month.splice(1,2);
/*	let nSplice = month.splice(1,2, `July`, `Aug`, `Sept`);
	console.log(month);
	console.log(nSplice);*/

	//Slice
		//returns a shallow copy of a portion of an array

		//1st param - index number (start)
		//2nd param - index number where to stop

		/*console.log(month);*/

		//let nSlice = month.slice(2); //from index 2 - last element
		//let nSlice = month.slice(2,4); //from index 2 -3
		//cut before index 4

		//Q: if you want to tcopy the elements from index 3 up to the last element, what values of params should we use?
		//nSlice = month.slice(3, month.length); 
			//same as month.slice(3);

	/*	let nSlice = month.slice(-4);*/
			//count starts from end of array if count is negative

/*		console.log(`month:`, month);
		console.log(`nSlice: `,nSlice);*/


	//Concat() method
		//merge 2 or more arrays
		//returns array, therefore need to assign to variable

/*		console.log(month);
		console.log(fruits);
		console.log(phones);

		let concatArray = month.concat(fruits, phones);
		console.log(concatArray);
		console.log(month);
		console.log(fruits);
		console.log(phones);*/

	//Join() method
		//Creates and returns a string by concatenating all elements in an array

			//parameters:
				//any that will be entered will be used as separator
				//" "
				//"-"
				//"," //default

/*		console.log(month);
		let newJoin = month.join();
		console.log(newJoin);*/
		//console.log(typeof newJoin);

	//toString() method
		//returns string

/*		let elements = [1, 2, `a`, `3b`];

		let nToString = elements.toString();
		console.log(nToString);
		//console.log(typeof nToString);

		let splitString = nToString.split(",")
		console.log(splitString);*/
		
/*
	Accessors
*/

	let countries = [`US`,`RU`,`CAN`,`PH`,`SG`,`HK`,`PH`]

	//indexOf()
		//we will find PH in an array
		//param - value
		//gets index first occurrence of indicated element

/*		console.log(countries.indexOf(`PH`));	//3
		console.log(countries.indexOf(`NZ`));*/

		//use as condition
/*		if (countries.indexOf(`NZ`) !== -1){
			alert(`PH is in the countries array`);
		} else{
			alert(`Country does not exist`);
		}*/

	//lastIndexOf()
		//param
/*		console.log(countries.lastIndexOf(`PH`));*/

	/*
		Iterators

			//forEach()
				//executes a provided function "once for each element"
				//does not return a value from the function

			Syntax of forEach:
			reference.forEach(callback fn)
		
				syntax of callback function:

				function(<parameter>){
					//statement
				}

				

				//console.log(month);

				month.forEach(
					function(element){
						console.log(element);
					}
				);

			//map()
				//returns a new copy of array, with different values depending on the result of the function's operation
				
				Syntax:
					refArray.map(
						function(param){
							//statement/codeblack
						}
					)
			*/
			let names = [`Angelito`, `Ian`, `Gerard`, `Mikhaella`, `Matthew`];

			let mapNames = names.map(
					function(student){
						//console.log(student)
						return(
							`
								<p>My name is ${student}</p>
							`
						);
					}
				).join(" ")

			//console.log(mapNames);

			let container = document.getElementById(`display`);

			container.innerHTML = mapNames;

/*
			//filter()
				//creates a new array that contains //elements which passed a given condition
				does not modify the original array

			syntax:

			refArray.filter(function){
				//statement
				return <condtion>
				}
			)
*/

			let nums = [1, 2, 3, 4, 5];

			let filteredNums = nums.filter(function(a){
					//console.log(a)

					return (a <= 3)
				}
			)

			//console.log(filteredNums);


		/*
			//includes
				//determines whether an array includes a certain value among its entries, returning true or false value
		*/

		let animals = [`dog`, `cat`, `bird`, `fish`];

		console.log(animals.includes(`dog`));

		/*
			Mini Activity

			using a function, return the word if the word exists in the array, else return animal not existing

		*/

/*		function animalsExist(word){
			if (animals.includes(word)){
				return word;
			}else {
				return `animal does not exist`;
			}
		}
		console.log(animalsExist(`giraffe`));*/

		/*
			//Every()
				//tests whether all elements in the array pass the test implemented by the provided function. It returns a Boolean value.
				//ALL
		*/

		console.log(nums)

		let result = nums.every(function(element){
			return element > 1
			}
		)

		console.log(result);

		/*
			//Some()
				//tests whether at least one element in the array passes the test implemented by the provided function. It returns true if, in the array, it finds an element for which the provided function returns true; otherwise it returns false. It doesn't modify the array
		*/

		let someResult = nums.some(function(element){
			return element > 1
			}
		)

		console.log(someResult);


		/*let nums = [1,2,3,4,5];

//Is it possible to get the total of these values from the array using array iterator forEach() method if this method doesn't return a value

let result = 0;

nums.forEach(function(digit){
				console.log(digit);

				//result = 0 + 1
				result += digit;
			}
)

console.log(result);*/